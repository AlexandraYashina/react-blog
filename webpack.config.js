const path = require('path');
HTMLplugin = require('html-webpack-plugin');

module.exports = {
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'main.js'
	},
	mode: 'development',
	module: {
		rules: [{
				test: /\.jsx?$/,
				use: "babel-loader",
				exclude: "/node_modules/"
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader']
			}
		]
	},
	plugins: [
		new HTMLplugin({
			template: path.resolve(__dirname, 'src', 'index.html'),
			filename: 'index.html'
		}),
	],
	devServer: {
		historyApiFallback: true,
		contentBase: path.resolve(__dirname, 'dist')
	}
}